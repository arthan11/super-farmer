import unittest
from consts import *
from superfarmer import SuperFarmer

class TestSuperFarmer(unittest.TestCase):
    def setUp(self):
        self.gra = SuperFarmer()
        self.gra.dodaj_gracza('Tomek')
        
    def test_krolik_0_plus_1(self):
        self.gra.rozpatrz_kostki(KROLIK, OWCA)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 0)

    def test_krolik_0_plus_2(self):
        self.gra.rozpatrz_kostki(KROLIK, KROLIK)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 1)

    def test_krolik_4_plus_1(self):
        self.gra.wez(KROLIK, 4)
        self.gra.rozpatrz_kostki(KROLIK, OWCA)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 6)

    def test_krolik_4_plus_2(self):
        self.gra.wez(KROLIK, 4)
        self.gra.rozpatrz_kostki(KROLIK, KROLIK)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 7)

    def test_krolik_4_lis(self):
        self.gra.wez(KROLIK, 4)
        self.gra.rozpatrz_kostki(KROLIK, LIS)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 0)

    def test_krolik_4_lis_maly_pies(self):
        self.gra.wez(KROLIK, 4)
        self.gra.wez(MALY_PIES, 1)
        self.gra.rozpatrz_kostki(KROLIK, LIS)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROLIK], 6)

    def test_owca_4_wilk(self):
        self.gra.aktywny_gracz.zagroda.zwierzeta[OWCA] = 4
        self.gra.rozpatrz_kostki(WILK, OWCA)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[OWCA], 0)

    def test_owca_4_wilk_duzy_pies(self):
        self.gra.wez(OWCA, 4)
        self.gra.wez(DUZY_PIES, 1)
        self.gra.rozpatrz_kostki(WILK, OWCA)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[OWCA], 6)

    def test_owca_bank(self):
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 1
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 1
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 2
        self.assertEqual(self.gra.bank.zwierzeta[OWCA], 20) # 24-1-1-2

    def test_owca_bank_wilk(self):
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 1
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 1
        self.gra.rozpatrz_kostki(OWCA, OWCA) # 2
        self.gra.rozpatrz_kostki(WILK, OWCA) # -4        
        self.assertEqual(self.gra.bank.zwierzeta[OWCA], 24) # 24-1-1-2+4

    def test_czy_wszystkie_zwierzeta_true(self):
        self.gra.wez(KROLIK, 1)
        self.gra.wez(OWCA, 1)
        self.gra.wez(SWINIA, 1)
        self.gra.wez(KROWA, 1)
        self.gra.wez(KON, 1)
        self.assertTrue(self.gra.aktywny_gracz.zagroda.czy_wszystkie_zwierzeta())

    def test_czy_wszystkie_zwierzeta_false(self):
        self.gra.wez(KROLIK, 0)
        self.gra.wez(OWCA, 0)
        self.gra.wez(SWINIA, 0)
        self.gra.wez(KROWA, 0)
        self.gra.wez(KON, 0)
        self.assertFalse(self.gra.aktywny_gracz.zagroda.czy_wszystkie_zwierzeta())

    def test_czy_mozna_wymienic_true(self):
        self.gra.wez(KROWA, 1)
        wymiana = ({KROWA: 1},{SWINIA:1, OWCA: 2, KROLIK: 12})
        self.assertTrue(self.gra.czy_mozna_wymienic(*wymiana))

    def test_czy_mozna_wymienic_false(self):
        self.gra.wez(KROWA, 1)
        wymiana = ({KROWA: 1},{SWINIA:1, OWCA: 1, KROLIK: 12})
        self.assertFalse(self.gra.czy_mozna_wymienic(*wymiana))

    def test_wymien_true(self):
        self.gra.wez(KROLIK, 40)
        wymiana = ({KROLIK: 36},{KROWA: 1})
        self.gra.wymien(*wymiana)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROWA], 1)

    def test_wymien_false(self):
        self.gra.wez(KROLIK, 30)
        wymiana = ({KROLIK: 36},{KROWA: 1})
        self.gra.wymien(*wymiana)
        self.assertEqual(self.gra.aktywny_gracz.zagroda.zwierzeta[KROWA], 0)

if __name__ == '__main__':
    unittest.main()
from random import randint
from consts import GREEN_DICE, RED_DICE

class Dice(object):
    def roll(self, count=1, typ=6):
        ret = 0
        for i in range(count):
            ret += randint(1, typ)
        return ret

class SuperFarmerDices(Dice):
    def roll(self):
        ret1 = super(SuperFarmerDices, self).roll(typ=12)
        ret2 = super(SuperFarmerDices, self).roll(typ=12)
        return GREEN_DICE[ret1-1], RED_DICE[ret2-1]

if __name__ == '__main__':
    dices = SuperFarmerDices()

    for i in range(10):
        r1, r2 = dices.roll()
        print r1.decode('utf-8'), r2.decode('utf-8')

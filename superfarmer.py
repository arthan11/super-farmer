# -*- coding: utf-8 -*-
import sys
from consts import *
from dice import SuperFarmerDices

class Stado(object):
    def __init__(self):        
        self.zwierzeta = {
            KROLIK: 0,
            OWCA: 0,
            SWINIA: 0,
            KROWA: 0,
            KON: 0,
            MALY_PIES: 0,
            DUZY_PIES: 0
        }

    def czy_wszystkie_zwierzeta(self):
        sum = 0
        for nazwa in ANIMALS_NEEDED:
            if self.zwierzeta[nazwa] > 0:
                sum += 1
        return sum == len(ANIMALS_NEEDED)

class Bank(Stado):
    def __init__(self):
        self.zwierzeta = {
            KROLIK: 60,
            OWCA: 24,
            SWINIA: 20,
            KROWA: 12,
            KON: 6,
            MALY_PIES: 4,
            DUZY_PIES: 2
        }
        
class Gracz(object):
    def __init__(self, imie):
        self.imie = imie
        self.zagroda = Stado()
        
    def __repr__(self):
        return self.imie

class SuperFarmer(object):
    def __init__(self):
        self.dices = SuperFarmerDices()
        self.bank = Bank()
        self.gracze = []
        self.aktywny_gracz = None
    
    def dodaj_gracza(self, imie):
        self.gracze.append(Gracz(imie))
        if not self.aktywny_gracz:
            self.aktywny_gracz = self.gracze[0]
    
    def nastepny_gracz(self):
        if self.gracze:
            pos = self.gracze.index(self.aktywny_gracz)
            pos += 1
            if pos >= len(self.gracze):
                pos = 0
            self.aktywny_gracz = self.gracze[pos]
    
    def oddaj(self, rodzaj, ilosc=1):
        self.aktywny_gracz.zagroda.zwierzeta[rodzaj] -= ilosc
        self.bank.zwierzeta[rodzaj] += ilosc

    def wez(self, rodzaj, ilosc=1):
        self.aktywny_gracz.zagroda.zwierzeta[rodzaj] += ilosc
        self.bank.zwierzeta[rodzaj] -= ilosc
    
    def wyrzucono_wilka(self):
        if self.aktywny_gracz.zagroda.zwierzeta[DUZY_PIES] > 0:
            self.oddaj(DUZY_PIES)
        else:
            for nazwa, wartosc in self.aktywny_gracz.zagroda.zwierzeta.items():
                if nazwa not in [KON, MALY_PIES, DUZY_PIES]:
                    self.oddaj(nazwa, wartosc)
        
    def wyrzucono_lisa(self):
        if self.aktywny_gracz.zagroda.zwierzeta[MALY_PIES] > 0:
            self.oddaj(MALY_PIES)
        else:
            wartosc = self.aktywny_gracz.zagroda.zwierzeta[KROLIK]
            self.oddaj(KROLIK, wartosc)
        
    def rzuc_kostki(self):
        return self.dices.roll()
    
    def rozpatrz_kostki(self, green, red):
        gracz = self.aktywny_gracz
        zwierzeta = gracz.zagroda.zwierzeta
        bank = self.bank.zwierzeta

        if green == WILK:
            self.wyrzucono_wilka()
        
        if red == LIS:
            self.wyrzucono_lisa()
            
        zwierzeta_1 = 0
        zwierzeta_2 = 0        
        if (green == red):
            zwierzeta_1 = zwierzeta[green] + 2
            zwierzeta_2 = 0
        else:
            if green in zwierzeta:
                zwierzeta_1 = zwierzeta[green] + 1
            if red in zwierzeta:
                zwierzeta_2 = zwierzeta[red] + 1
        zwierzeta_1 = zwierzeta_1 / 2
        zwierzeta_2 = zwierzeta_2 / 2
        
        if (zwierzeta_1 > 0):
            zwierzeta_1 = min(zwierzeta_1, bank[green])
            self.wez(green, zwierzeta_1)
            
        if (zwierzeta_2 > 0):
            zwierzeta_2 = min(zwierzeta_2, bank[red])
            self.wez(red, zwierzeta_2)
            
    def czy_mozna_wymienic(self, zwierz1, zwierz2):
        # po jednej ze stron musi byc pojedyncze zwierze
        if len(zwierz1) > 1 and len(zwierz2) > 1:
            return False

        # czy aktywny gracz posiada dane zwierzeta i sumowanie ich wartosci
        sum1 = 0
        for nazwa, ilosc in zwierz1.items():
            if self.aktywny_gracz.zagroda.zwierzeta[nazwa] < ilosc:
                return False
            sum1 += KOSZT[nazwa] * ilosc
            
        # czy w banku sa wymienione zwierzeta i sumowanie ich wartosci
        sum2 = 0
        for nazwa, ilosc in zwierz2.items():
            if self.bank.zwierzeta[nazwa] < ilosc:
                return False
            sum2 += KOSZT[nazwa] * ilosc
            
        return sum1 == sum2

    def wymien(self, zwierz1, zwierz2):
        if not self.czy_mozna_wymienic(zwierz1, zwierz2):
            return False
            
        for nazwa, ilosc in zwierz1.items():
            self.oddaj(nazwa, ilosc)
        for nazwa, ilosc in zwierz2.items():
            self.wez(nazwa, ilosc)
        return True

if __name__ == '__main__':
    gra = SuperFarmer()
    print gra.bank.zwierzeta
    gra.dodaj_gracza('Jaś')
    #gra.dodaj_gracza('Małgosia')
    #gra.dodaj_gracza('Tomek')
    print gra.gracze
    for i in range(4):
        gracz = gra.aktywny_gracz
        print gracz.imie.decode('utf-8')
        # wymiana
        
        #green, red = gra.rzuc_kostki()
        #gra.rozpatrz_kostki(green, red)
        gra.rozpatrz_kostki(KROLIK, KROLIK)
        print gracz.zagroda.zwierzeta
        print gra.bank.zwierzeta
        if gracz.zagroda.czy_wszystkie_zwierzeta():
            print '*'*10+ ' WINNER!! ' + '*'*10
            sys.exit()
        
        gra.nastepny_gracz()
    
    wymiana = ({KROLIK:6}, {OWCA: 1})     
    print gra.czy_mozna_wymienic(*wymiana)
    print gra.wymien(*wymiana)
    print gra.aktywny_gracz.zagroda.zwierzeta
    print gra.bank.zwierzeta
        
# -*- coding: utf-8 -*-

WILK = 'wilk'
KROWA = 'krowa'
SWINIA = 'świnia'
OWCA = 'owca'
KROLIK = 'królik'
LIS = 'lis'
KON = 'koń'
MALY_PIES = 'mały pies'
DUZY_PIES = 'duży pies'

GREEN_DICE = [WILK, KROWA, SWINIA] + [OWCA]*3 + [KROLIK]*6
RED_DICE = [LIS, KON] + [SWINIA]*2 + [OWCA]*2 + [KROLIK]*6

ANIMALS_NEEDED = [KROLIK, OWCA, SWINIA, KROWA, KON]

KOSZT = {
    KROLIK: 1,
    OWCA: 6,
    SWINIA: 12,
    KROWA: 36,
    KON: 72,
    MALY_PIES: 6,
    DUZY_PIES: 36,
}